# Question2Answer es-ES Spanish (Spain) translation

## Installation 

1. [Download the package](https://gitlab.com/midget/q2a-translation-es-ES/tags) matching your Q2A installed version.
2. Copy the directory `es-ES` into `qa/qa-lang` directory.
3. Select the `Español (España)` translation from the **Admin** section.

## Contribute
If you want to contribute, please read about [Contributing to GitLab](https://about.gitlab.com/contributing) first.

## Issues
Please, report issues via [GitLab bugtracker](https://gitlab.com/midget/q2a-translation-es-ES/issues)

## License

This software is published under GPLv2 License.
Read [LICENSE file](https://gitlab.com/midget/q2a-translation-es-ES/blob/master/LICENSE) for more info or visit [GNU General Public License, version 2](https://www.gnu.org/licenses/old-licenses/gpl-2.0.en.html) official page.














